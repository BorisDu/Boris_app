Template.home.rendered=function(){

    var dateGap = -9;
    var minDate = "2016-07-01";
    var endDate = new Date().getNewDate(-1);
    var startDate = new Date(endDate).getNewDate(dateGap);

    $(".dateSelectLabel").html(startDate+"~"+endDate);
    $('.webTrafficFunnelDate').daterangepicker(datePickerOptionsFunc(startDate,endDate,minDate,false), pickDateRangeCallback);

    getValue({"startDate": startDate, "endDate": endDate});
    
    $(".country").change(function () {
        var startDate=$('.dateSelectLabel').html().split("~")[0];
        var endDate=$('.dateSelectLabel').html().split("~")[1];
        getValue({"startDate": startDate, "endDate": endDate});
    })
};

function pickDateRangeCallback(start, end,label) {
    console.log("New date range selected: " + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD') + " (predefined range: " + label + ")");
    var sdt = start.format('YYYY-MM-DD');
    var edt = end.format('YYYY-MM-DD');
    $('.dateSelectLabel').html(sdt+"~"+edt);
    getValue({"startDate": sdt, "endDate": edt});
}

function getValue(filter){
    $(".country").append("<option value='美元'>美元</option>");
    requestURL(dataService+"/exchange/getExchangeFilterOption",{}).done(function(data){
        data.toCountry.forEach(function(e){
            $(".country").append("<option value='"+e+"'>"+e+"</option>");
        })
    })
    filter.country=$(".country").val();
    requestURL(dataService+"/exchange/getExchangeValue",filter).done(function(data){
        drawRadialGradient(data);
    })
}

function drawRadialGradient(ret){
    var data ={
        date:[],
        value:[]
    }
    ret.forEach(function (e) {
        data.date.push(e.date);
        data.value.push(e.value);
    })
    var option = {
        title: {
            // text: '折线图堆叠'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data:['汇率']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: data.date
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name:'汇率',
                type:'line',
                data:data.value
            }
        ]
    };

    var RadialGradient=echarts.init(document.getElementById('line-chart'));
    RadialGradient.setOption(option);
}
