Template._appHeader.helpers({
    currentProcessCount: function () {
        if (Meteor.user().profile.name != undefined) {
            var stat = obProcessCount.findOne({ob_id: Meteor.user().profile.name});
            if (stat != undefined) {
                return stat.count
            } else {
                return null
            }
        } else {
            return null
        }
    },
    IPCount: function () {
        return obIPRecord.find({isWhite: false}).count()
    },
    orderCount: function () {
        var stat = dateTradeStats.findOne({}, {sort: {createdDt: -1}});
        if (stat != undefined) {
            return stat.tradeNum
        } else {
            return 0
        }
    },
    dealCount: function () {
        var stat = dateTradeDealStats.findOne({}, {sort: {createdDt: -1}});
        if (stat != undefined) {
            return stat.tradeNum
        } else {
            return 0
        }
    }
});
Template._appHeader.rendered = function () {
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;
    var isiOS = ua.indexOf("iphone") > -1;
    setTimeout(function () {
        if (isAndroid || isiOS) {
            $('#mobilenote').show();
            $('#crm').hide()
        } else {
            $('#mobilenote').hide();
            $('#crm').show();
        }
    }, 1000);

    hideOwnNaviTab();

};