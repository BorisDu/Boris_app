/**
 * Created by dushi on 2016/12/11.
 */

urlParamsProcess=function (query) {
    var params=Object.keys(query).map(function(k){
        var val=query[k];
        if(val instanceof Array){
            var retStr="";
            val.forEach(function (ele) {
                retStr+=k+encodeURIComponent('[]')+'='+encodeURIComponent(ele)+"&"
            });
            if(retStr){
                return retStr.substring(0,retStr.length-1)
            }else{
                return k+encodeURIComponent('[]')+'='
            }
        }else{
            return encodeURIComponent(k)+'='+encodeURIComponent(val)
        }
    }).join('&');
    return params
}

requestURL=function(path,query){
    if(Meteor.isClient){
        var params=urlParamsProcess(query);
        var url=path+"?"+params;
        var dfd=$.Deferred();
        $.getJSON(path,query,function (result) {
            dfd.resolve(result)
        })
        return dfd.promise();
    }
}

///基本的常用工具
var moment=require('moment');

function transformDate(date) {
    var dateStrArray = date.split('-');
    return dateStrArray[1] + '/' + dateStrArray[2] + '/' + dateStrArray[0]
};
cleanParams = function(filter){
    var query = _.clone(filter);
    for(var key in query){
        if(!query[key]&&key!="offset"){
            delete query[key]
        }
    }
    return query
}

Date.prototype.getNewDate=function (count) {

    this.setDate(this.getDate() + count);
    var year = this.getFullYear();
    var month = this.getMonth() + 1;
    var day = this.getDate();
    if (month < 10) {
        month = "0" + month
    }
    if (day < 10) {
        day = "0" + day
    }
    return year + "-" + month + "-" + day;
}

datePickerOptionsFunc =function(startDate,endDate,minDate,isSingleFlag) {
    return {
        "showDropdowns": true,
        //"alwaysShowCalendars": true,
        //"singleDatePicker": true,
        "singleDatePicker": isSingleFlag==undefined?false:isSingleFlag,
        "autoApply": true,
        "alwaysShowCalendars": false,
        "startDate": transformDate(startDate),
        "endDate": transformDate(endDate),
        "minDate": transformDate(minDate),
        "maxDate": transformDate(endDate),
        "locale": {
            "format": "MM/DD/YYYY",
            "separator": " - ",
            "applyLabel": "确认",
            "cancelLabel": "取消",
            "fromLabel": "从",
            "toLabel": "至",
            "customRangeLabel": "自定义",
            "daysOfWeek": [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
            ],
            "monthNames": [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
            ],
            "firstDay": 0
        },
        "ranges": {
            '昨天': [moment().subtract(1, 'days').toDate(), moment().subtract(1, 'days').toDate()],
            '最近7天': [moment().subtract(7, 'days').toDate(), moment().toDate()],
            '最近15天': [moment().subtract(15, 'days').toDate(), moment().toDate()],
            '最近30天': [moment().subtract(30, 'days').toDate(), moment().toDate()],
            '本周': [moment().startOf('week').toDate(), moment().endOf('week').toDate()],
            '上周': [moment().subtract(1, 'week').startOf('week').toDate(), moment().subtract(1, 'week').endOf('week').toDate()],
            '本月': [moment().startOf('month').toDate(), moment().endOf('month').toDate()],
            '上个月': [moment().subtract(1, 'month').startOf('month').toDate(), moment().subtract(1, 'month').endOf('month').toDate()],
            '今年': [moment().startOf('year').toDate(), moment().endOf('year').toDate()],
            '去年': [moment().subtract(1, 'year').startOf('year').toDate(), moment().subtract(1, 'year').endOf('year').toDate()]
        }
    }
};
weekDatePickerOptionsFunc=function(startDate,endDate,minDate,isSingleFlag){
    return {
        onlyShowfirstDayOfWeek: true,
        "showDropdowns": true,
        //"alwaysShowCalendars": true,
        "alwaysShowCalendars": false,
        "singleDatePicker": isSingleFlag==undefined?false:isSingleFlag,
        "autoApply": true,
        "startDate": transformDate(startDate),
        "endDate": transformDate(endDate),
        "minDate": transformDate(minDate),
        "maxDate": transformDate(endDate),
        "locale": {
            "format": "MM/DD/YYYY",
            "separator": " - ",
            "applyLabel": "确认",
            "cancelLabel": "取消",
            "fromLabel": "从",
            "toLabel": "至",
            "customRangeLabel": "自定义",
            "daysOfWeek": [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
            ],
            "monthNames": [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
            ],
            "firstDay": 0
        },
        "ranges": {
            '上周': [moment().subtract(1, 'week').startOf('week').toDate(), moment().subtract(1, 'week').startOf('week').toDate()],
            '上5周': [moment().subtract(5, 'week').startOf('week').toDate(), moment().subtract(1, 'week').startOf('week').toDate()],
            '上10周': [moment().subtract(10, 'week').startOf('week').toDate(), moment().subtract(1, 'week').startOf('week').toDate()],
            '上15周': [moment().subtract(15, 'week').startOf('week').toDate(), moment().subtract(1, 'week').startOf('week').toDate()],
            '今年': [moment().startOf('year').startOf('week').toDate(), moment().subtract(1, 'week').startOf('week').toDate()],
            '去年': [moment().subtract(1, 'year').startOf('year').startOf('week').toDate(), moment().subtract(1, 'year').endOf('year').startOf('week').toDate()]
        }
    }
};
monthDatePickerOptionsFunc=function(startDate,endDate,minDate,isSingleFlag){
    return {
        "onlyShowfirstDayOfMonth":true,
        "showDropdowns": true,
        //"alwaysShowCalendars": true,
        "singleDatePicker": isSingleFlag==undefined?false:isSingleFlag,
        "alwaysShowCalendars": false,
        "autoApply": true,
        "startDate": transformDate(startDate),
        "endDate": transformDate(endDate),
        "minDate": transformDate(minDate),
        "maxDate": transformDate(endDate),
        "locale": {
            "format": "MM/DD/YYYY",
            "separator": " - ",
            "applyLabel": "确认",
            "cancelLabel": "取消",
            "fromLabel": "从",
            "toLabel": "至",
            "customRangeLabel": "自定义",
            "daysOfWeek": [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
            ],
            "monthNames": [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
            ],
            "firstDay": 0
        },
        "ranges": {
            '本月':[moment().startOf('month').toDate(),moment().startOf('month').toDate()],
            '上月': [moment().subtract(1, 'month').startOf('month').toDate(), moment().subtract(1, 'month').startOf('month').toDate()],
            '上3月': [moment().subtract(3, 'month').startOf('month').toDate(), moment().subtract(1, 'month').startOf('month').toDate()],
            '上6月': [moment().subtract(6, 'month').startOf('month').toDate(), moment().subtract(1, 'month').startOf('month').toDate()],
            '上12月': [moment().subtract(12, 'month').startOf('month').toDate(), moment().subtract(1, 'month').startOf('month').toDate()],
            '今年': [moment().startOf('year').startOf('month').toDate(), moment().subtract(1, 'month').startOf('month').toDate()],
            '去年': [moment().subtract(1, 'year').startOf('year').startOf('month').toDate(), moment().subtract(1, 'year').endOf('year').startOf('month').toDate()],
        }
    }
}